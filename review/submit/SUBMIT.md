
Project Review
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment       | Project Review
-------------------- | --------------------------------
Name                 |
Net ID               |
Assigned project (#) | 
Review due           | Monday, 4 May 11:59PM


Please write detailed comments answering the questions below.
Do not just answer "yes" or "no" - comment on *how well* the authors
address each of these aspects of experimentation, and offer 
suggestions for improvement.

## Overview

1) Briefly summarize the experiment in this project.



2) Does the project generally follow the guidelines and parameters we have 
learned in class? 




## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?


2) Are the metric(s) and parameters chosen appropriate for this 
experiment goal? Does the experiment design clearly support the experiment goal?


3) Is the experiment well designed to obtain maximum information with the 
minimum number of trials?


4) Are the metrics selected for study the *right* metrics? Are they clear, 
unambiguous, and likely to lead to correct conclusions? Are there other 
metrics that might have been better suited for this experiment?


5) Are the parameters of the experiment meaningful? Are the ranges 
over which parameters vary meaningful and representative?


6) Have the authors sufficiently addressed the possibility of interactions 
between parameters?



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate 
and realistic? 



## Communicating results


1) Do the authors report the quantitative results of their experiment?


2) Is there information given about the variation and/or distribution of 
experimental results?


3) Do the authors practice *data integrity* - telling the truth about their data, 
avoiding ratio games and other practices to artificially make their results seem better?


4) Is the data presented in a clear and effective way? If the data is presented in 
graphical form, is the type of graph selected appropriate for the "story" that 
the data is telling? 


5) Are the conclusions drawn by the authors sufficiently supported by the 
experiment results?

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, 
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?


2) Were you able to successfully produce experiment results? 


3) How long did it take you to run this experiment, from start to finish?


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?



## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.











